import React, {Component} from 'react';
import {connect} from "react-redux";
import './AccessCode.css';
import {enter, clear, addNumber} from "../../store/actions";


class AccessCode extends Component {

    render() {
        return (
            <div className="Code">
                <h1 >{new Array(this.props.ctr.length).fill('*').join('')}</h1>
                <h2 style={{background: this.props.color ? 'green' : 'red'}}>{this.props.message}</h2>
                <button onClick={() => this.props.addNumber(1)}>1</button>
                <button onClick={() => this.props.addNumber(2)}>2</button>
                <button onClick={() => this.props.addNumber(3)}>3</button>
                <button onClick={() => this.props.addNumber(4)}>4</button>
                <button onClick={() => this.props.addNumber(5)}>5</button>
                <button onClick={() => this.props.addNumber(6)}>6</button>
                <button onClick={() => this.props.addNumber(7)}>7</button>
                <button onClick={() => this.props.addNumber(8)}>8</button>
                <button onClick={() => this.props.addNumber(9)}>9</button>
                <button onClick={() => this.props.clear()}>C</button>
                <button onClick={() => this.props.addNumber(0)}>0</button>
                <button onClick={() => this.props.enter(this.props.ctr)}>E</button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.code,
        message: state.message,
        color: state.color
    };
};

const mapDispatchToProps = dispatch => {
    return {
        addNumber: (number) => dispatch(addNumber(number)),
        enter: (number) => dispatch(enter(number)),
        clear: () => dispatch(clear()),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccessCode);

