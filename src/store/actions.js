export const ENTER = 'ENTER';
export const CLEAR = 'CLEAR';
export const ADD_NUMBER = 'ADD_NUMBER';

export const clear = () => {
    return (dispatch) => {
        dispatch({ type: CLEAR });
    };
};

export const addNumber = (number) => {
    return {type: ADD_NUMBER, number: number}
};

export const enter = number => {
    return (dispatch) => {
        dispatch({type: ENTER, number: number});
    };
};




