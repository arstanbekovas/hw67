import {ENTER, CLEAR, ADD_NUMBER} from "./actions";

const trueCode = '6547';

const initialState = {
    code: '',
    message: '',
    color: false
};

const reducer = (state = initialState, action) => {
    if (action.type === ADD_NUMBER) {
        if (state.code.length < 4) {
            return {...state, code: state.code + action.number}
        }
    }

    if (action.type === ENTER) {
        if (state.code === trueCode) {
            return {...state, message: 'Access granted', color: true};
        } else {
            return {...state, message: 'Access denied', color: false};
        }
    }

    if (action.type === CLEAR) {
        return {...state, code: state.code.slice(0, -1)}
    }
    return state;

};



export default reducer;
